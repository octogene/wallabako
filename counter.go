package main

import (
	"sync"
)

// SafeCounter is safe to use concurrently.
// cargo-culted from https://tour.golang.org/concurrency/9
type SafeCounter struct {
	v   map[string]int
	mux sync.Mutex
}

// Inc increments the counter for the given key.
func (c *SafeCounter) Inc(key string) {
	c.mux.Lock()
	defer c.mux.Unlock()
	c.v[key]++
}

// Add adds the value to the given key.
func (c *SafeCounter) Add(key string, value int) {
	c.mux.Lock()
	defer c.mux.Unlock()
	c.v[key] += value
}

// Value returns the current value of the counter for the given key.
func (c *SafeCounter) Value(key string) int {
	c.mux.Lock()
	defer c.mux.Unlock()
	return c.v[key]
}
